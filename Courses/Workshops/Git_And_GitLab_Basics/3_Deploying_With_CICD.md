### Theme

Use GitLab CICD templates to enhance our already existing cicd pipeline and see how the changes will wrap back up into our merge request

### Key Tasks to Complete

# Step 1: Enable Auto DevOps

1. First navigate to our VSC view and click into the **.gitlab-ci.yml** file. Right away we can see that we have a pipeline defined, however This only builds our application and runs a unit test. We will be able to upgrade this by including a single template. 

2. We will then want to make sure we are **not** using the main branch, so in our open terminal run the below command and ensure its still our MR branch:

  ```
  git rev-parse --abbrev-ref HEAD
  ```
  
3. Go back to our file view and delete all of the yaml code currently in our config, Auto DevOps will be able to replace this code and more.

4. Go ahead and add the current _stage_ config with the text below: 

    ```
    include:
        - template: Auto-DevOps.gitlab-ci.yml

    stages:
      - build
      - test
      - dast
      - review
      - cleanup
      - staging
      - canary
      - production
      - incremental rollout 10%
      - incremental rollout 25%
      - incremental rollout 50%
      - incremental rollout 100%
      - performance
      - deploy

    ```

    </br>

    We disable the Review app here because we will not be fully deploying out an application in this workshop, but you can learn more about the review app [here](https://docs.gitlab.com/ee/ci/review_apps/) 

    </br>

5. Now that we have our code written we will commit it back to our GitLab repo. Lets use the terminal to run the command below to stage our changes:


  ```
  git add .
  ```

6. Next we want to actually commit our code to the local branch with a short commit message:

  ```
  git commit -a -m "adding Auto DevOps"
  ```

7. Lastly we will push it up to GitLab using the below command:

  ```
  git push
  ```

# Step 2: GitLab Pipeline Editor

1. Now that we have pushed out changes to GitLab lets navigate to the GitLab view. We first are going to check in on the pipeline that we kicked off with our push. Use the left hand navigation menu to click through **Build>Pipelines** and confirm that a pipeline is running. 

2. Notice that there are a number of different jobs all kicked off with Auto DevOps. To learn more about what we have ran we are going to use the left hand navigation menu to click through **Build>Pipeline Editor**.

3. First in the top right click **main** to swap to our merge request branch, and click the tree icon so that you can see a list of all included templates we brought in through Auto DevOps.

4. Now your template list view should have a number of deployment, security, and build templates added. You can click an individual template to see what was included, or click the **Full configuration** tab to get a full view of what our cicd really looks like once all the templates are added. 

5. We can also click the **Visualize** tab to get a visual mapping of the current layout of our CICD pipeline.
  
6. If we had any issues in our writing of the pipeline you could click the **Validate** tab to get an exact readout of where their may be issues in your config.

> [Docs on Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)
<br>

7. Let's check back in on our pipeline using the left hand navigation menu to click through **Build > Pipelines**. Once on the pipeline view you can also click into the pipeline to see all the jobs that are running. Notice that it isn't the same amount of jobs that we saw in the visualize tab because some jobs were not run based on the rules defined on them, specifically that we do not have a deployment path defined or cluster hooked up. We will go to a quick break while we wait for this to run.

# Step 3: Root Cause Analysis

1. Now that our pipeline has ran notice that there are two jobs that have already failed! click into either the _code_intelligence_go_ job.
  
2. Notice that there are plenty of logs that you could parse through to potentially troubleshoot why these jobs failed, or click **Troubleshoot** for gitlab to tell you why they failed.
  
3. After reading the generated prompt share in the chat why you now feel like your automatically ran job has failed.
  
4. Next we want to use the left hand navigation menu to click through **Code > Merge request** and then click into our existing merge request.

> [Docs on Shifting Left](https://about.gitlab.com/topics/ci-cd/shift-left-devops/)
<br>
